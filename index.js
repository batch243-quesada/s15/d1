// console.log('Hello World');

// [Section] Syntax, Statements, and Comments
// Statements in programming are instructions that we tell the computer to perform.
// Js statements usually end with semicolon (;).
// Semicolons are not required in JS, but we will use it to help us train to locate where a statement ends.
// A syntax in programming is the set of rules of rules that we describe how statements must be constructed.
// All lines/blocks of code should be written in specific manner to work. This is due to how these codes where initially programmed to function in certain manner.


// [Sections] Variables
// Variables are used to contain data.
// Any information that is used by an application is stored in what we call the "memory."
// When we create variables, certain portions of a device's memory are given names that we call variables.
// This makes it easier for us to associate information stored in our devices to actual "names" about information.
// Declaring variables
	// it tells our devices that a variable name is created and is ready to store data.
let name = 'Ada Lovelace';
// console.log() is useful for printing values of variables or certain results of code into the Google Chrome browser's console.
	// Constant use of this throughout developing an application will save us time and builds good habit in alwayas checking for the output of our code.
console.log(name);


// Guides in writing variables:
	// 1. use the let keyword followed the variable name of your choice and use the assignment operator (=) to assign value.
	// 2. Variable names should start with lowercase character, use camlCase for multiple words.
	// 3. For constant variables, use the 'const' keyword.
		// using let: we can change the value of the variable.
		// using const: we cannot change the value of the variable.
	// 4. Variable names should be indicative (descriptive) of the value being stored to avoid confusion.

// Declare and Initialize variables
// Initializing variables - the instance when a variable is given it's initl=al/starting value
	// Syntax
		// let/const variableName = value:

// Reassigning variable values
// Reassigning a variable value means changing it's inital or previous value into another value.
	// Syntax
		// let variableName = value;
		// variableName = newValue;

// Reassigning variables and initializing variables


// Scopes
	// let/const local/global scope
	// Scope essentially means where these variables are available 			for use
	// let and const are block scoped.
	// A block is a chunk of code bounded by {}. A block in curly braces. Anything wihtin the curly brace is a blcok

let oV = 'hello';
{
	let iV = 'hi';
	let oV = 'yea';
	console.log(iV);
	console.log(oV);
}

console.log(oV);


// [Section] Data Types
// Strings - are series of characters that creat a word, a phrase, a sentence or anything related to creating text.
	// Strings in JS can be enclosed either with single quotes ' ' or double " " quoteation marks.
	// In other programming languages, only the double quotes can be used to enclose strings.

let firstName = 'chris';
let lastName = 'qiu';
console.log('Hi', firstName);
console.log(`Hi ${firstName} ${lastName}`);

let age = 25;
let grade = '50';
let num = age + grade;

console.log(num);
console.log('You are', age);
console.log('You are ' + age);

// Arrays
// special kind of data type that's used to store multiple values.
// Arrays can store different data types but is mormally used to store similar data types.

// Objects
// are another special kind of data type that's used to mimic real world objects/items
// they're used to create a complex data that contains pieces of information that are relevant to each other
// Syntax
	// let/const objectName = {
		// propertyA: value,
		// propertyB: value
	//}

let person = {
	fullName: 'Juan',
	age: 25,
	isMarried: false,
	contact: ["09171234567", "81234567"],
	address: {
		houseNumber: '123',
		city: 'Cebu'
	}
}

console.log(person);
console	.log(person.age);
console.log(person.contact[1]);
console.log(person.address.city);

// typeof operator is used to determine the type or data the value of a variable

console.log(typeof person);

// Note: array is a special type of object with methods and functions to manipulate it.
console.log(typeof details);

// Constant Objects and Arrays
// The keyword const is a little misleading.

// It does not define a constant value. It defines a constant reference to a value.

// Because of this you can NOT:

// Reassign a constant value
// Reassign a constant array
// Reassign a constant object

// But you CAN:

// Change the elements of constant array
// Change the properties of constant object

const anime = ['naruto', 'one piece'];
console.log(anime);

anime[0] = 'snk'
console.log(anime);